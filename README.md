# Duke Unified Development Standards #
As student and front-facing development becomes more and more important, the need for a standard set of development practices becomes increasingly clear. We need a way to all be on the same page, so the DUDS were born. This repository should hold the current practices documentation. It will attempt to discuss strategy from the highest to the lowest levels. 
   
## Perpetual Goals ##
The goals below, listed in order of priority, are intended to function as guiding principles without significant change for the duration of this document's lifetime. 
- Making this document easy to read (both in length and in difficulty)
- Providing opinionated standards that do not conflict with each other
- Describing sustainable development strategies that can be easily taught to  and read by future coders
- Describing practices that do not restrict the types of things that can be made
- Describing practices at the highest (design level) and the lowest (language, functional levels) to remove ambiguity
- Describing practices that encourage development with high performance and low overhead

## Frameworks and Overhead ##
Don't use JQuery

## Languages and Development Environments ##
Typescript/Javascript with VSCode

## Web Services ##
API interfaces with Static frontends

## Security ##
Delegate upwards
